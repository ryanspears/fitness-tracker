// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDjTrpP8MGLukKVadgDwG-I4ekT6OvltnI",
    authDomain: "ng-fitness-tracker-rjs.firebaseapp.com",
    databaseURL: "https://ng-fitness-tracker-rjs.firebaseio.com",
    projectId: "ng-fitness-tracker-rjs",
    storageBucket: "ng-fitness-tracker-rjs.appspot.com",
    messagingSenderId: "958018466261"
  }
};
