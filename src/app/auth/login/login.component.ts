import { Component, OnInit } from "@angular/core";
import { AuthService } from "../auth.service";
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";
import { UiService } from "../../shared/ui.service";
import { Subscription } from "rxjs/Subscription";
import { Store } from "@ngrx/store";
import * as fromRoot from "../../app.reducer";
import { Observable } from "rxjs/internal/Observable";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  isLoading$: Observable<boolean>;
  private loadingSubs: Subscription;

  constructor(private authService: AuthService, private uiService: UiService, private store: Store<fromRoot.State>) {}

  ngOnInit() {
    this.isLoading$ = this.store.select(fromRoot.getIsLoading);
  }

  onSubmit(form: NgForm) {
    this.uiService.loadingStateChanged.next(true);
    this.authService.login({
      email: form.value.email,
      password: form.value.password
    });
  }
}
